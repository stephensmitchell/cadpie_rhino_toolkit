﻿Imports System
Imports System.Collections.Generic
Imports Rhino
Imports Rhino.Commands
Imports Rhino.Geometry
Imports Rhino.Input
Imports Rhino.Input.Custom

Namespace Export_as_fbx

    <System.Runtime.InteropServices.Guid("6400f828-a302-4338-b027-9fdf9611610e")> _
    Public Class ExportasfbxCommand
        Inherits Command

        Shared _instance As ExportasfbxCommand 

        Public Sub New()
            ' Rhino only creates one instance of each command class defined in a
            ' plug-in, so it is safe to store a refence in a static field.
            _instance = Me
        End Sub

        '''<summary>The only instance of this command.</summary>
        Public Shared ReadOnly Property Instance() As ExportasfbxCommand
            Get
                Return _instance
            End Get
        End Property

        '''<returns>The command name as it appears on the Rhino command line.</returns>
        Public Overrides ReadOnly Property EnglishName() As String
            Get
                Return "ExportasfbxCommand"
            End Get
        End Property

        Protected Overrides Function RunCommand(ByVal doc As RhinoDoc, ByVal mode As RunMode) As Result
            ' TODO: start here modifying the behaviour of your command.
            ' ---

            RhinoApp.SendKeystrokes("-import", False)
            RhinoApp.SendKeystrokes("", True)
            RhinoApp.SendKeystrokes("", True)
            doc.Views.Redraw()

            '' ---

            Return Result.Success
        End Function
    End Class
End Namespace